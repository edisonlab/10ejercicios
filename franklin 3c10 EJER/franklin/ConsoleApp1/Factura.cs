﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicios
{
    //3.	Crear un programa que lea cantidades y precios y al final indique el total de la factura

    class Factura
    {
        private int cantidad;
        private double precio;
        private int producto;
        private double total;
        public Factura()
        {
            cantidad = 1;
            precio = 0.0;
            producto = 1;
            total = 0.0;
        }
        public void FacturaF()
        { 
            while (cantidad != 0)
            {
                Console.WriteLine("PRODUCTO {0}", producto);
                Console.WriteLine("Introduzca la cantidad vendida ");

                cantidad = int.Parse(Console.ReadLine()); 
                if (cantidad != 0)
                {
                    Console.WriteLine("Introduzca el precio ");
                    precio = double.Parse(Console.ReadLine());

                    total = total + (cantidad * precio);
                    producto++;
                    Console.WriteLine(" ------------ ");
                }
                else
                {
                    break;
                }
            }

            //Console.WriteLine("==========");
            Console.WriteLine("total $ {0}", total);
        }


       

    }
}

