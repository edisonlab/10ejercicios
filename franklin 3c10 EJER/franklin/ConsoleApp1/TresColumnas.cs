﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicios
{
    //8.	Crear un programa en C# que lea un número entero y positivo y que escriba tres columnas. La primera cuenta desde uno hasta el número escrito
    //    contando de uno en uno; la segunda columna cuenta de dos en dos y la tercera de tres. 
    class tresColumnas
    {
        private int numero;
        public tresColumnas()
        {
            Console.WriteLine("ingrese un número");
            numero = int.Parse(Console.ReadLine());
        }
        public void Colum3 ()
        {
            Console.WriteLine("Aumento de uno en uno");

            for (int i = 1; i <= numero; i++)
            {
                Console.WriteLine(i);

            }
            Console.WriteLine();
            Console.WriteLine("Aumento de dos en dos");
            for (int i = 1; i <= numero; i+=2)
            {
                Console.WriteLine(i );
            }
            Console.WriteLine();
            Console.WriteLine("Aumento de tres en tres");
            for (int i = 1; i <= numero; i += 3 ) 
            {
                Console.WriteLine(i);
            }
        }
    
    }
}
