﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicios
{
    //7.	Crear un programa que escriba dos columnas de números, en la primera se colocan los números del 1 al 100, 
    //    en la segunda los números del 100 al 1 
    class DosColumnas
    {
      
        private int v = 100;

        public void ColumA()
        {
            Console.WriteLine("columna 1    columna 2");

            for (int i = 1; i <= 100; i++)
            { 
                Console.WriteLine(i+"            "+v);
                v = 100 - i;
            }
        }
        
    }
}
