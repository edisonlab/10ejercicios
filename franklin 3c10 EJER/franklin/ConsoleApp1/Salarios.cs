﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicios
{
    //4.	Crear un programa para calcular el salario semanal de unos empleados a los que se les paga 15 dólares por 
    //    hora si éstas no superan las 35 horas.Cada hora por encima de 35 se considerará extra y se paga a 22 dólares.
    class Salario
    {
        private int numeroDeHoras;
        private static int precioPorHoras;
        private static int precioPorHorasExtras;
        private String nombre;
        private int sueldo;
        public Salario()
        {
            precioPorHoras = 15;
            precioPorHorasExtras = 22;
            numeroDeHoras = 0;
            nombre = " ";
            sueldo = 0;
        }
        public void ConsultaSalario()
        {
            Console.WriteLine("Ingrese el nombre");
            nombre = Console.ReadLine();
            Console.WriteLine("Ingrese el numero de horas trabajadas por {0}", nombre);
            numeroDeHoras = int.Parse(Console.ReadLine());
            CalcularSueldo();
        }
        public void CalcularSueldo()
        {
            int diferencia = 0;
            if (numeroDeHoras > 35)
            {
                diferencia = (numeroDeHoras - 35);
                sueldo = 35*precioPorHoras;
                sueldo = sueldo + (diferencia * precioPorHorasExtras);
                Console.WriteLine(" el sueldo de esta semana de {0} es {1}", nombre, sueldo);
                Pregunta();
            }
            else
            {
                sueldo = numeroDeHoras * precioPorHoras;
                Console.WriteLine(" el sueldo de esta semana de {0} es {1}", nombre, sueldo);
                Pregunta();
            }
        }
        public void Pregunta()
        {
            int num ;
            Console.WriteLine("----------------");
            Console.WriteLine("si desea calcular otro salario inserte el número 1.");
            Console.WriteLine("caso contrario inserte otro número.");
            num = int.Parse(Console.ReadLine());

            if(num  == 1)
            {
                ConsultaSalario();
            }
            else
            {
                Console.WriteLine("Disfrute su día");
            }
        }
           
    
    }
    
}
