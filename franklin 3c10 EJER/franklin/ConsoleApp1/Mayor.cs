﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicios
{

    //1.	Programa que lea una serie de números por teclado e indique cuál es el mayor
    class Mayor
    {
        private static int tamaño;
        private int[] numeros;
        private int mayor;

       public Mayor() {
            Console.WriteLine("Cuantos números va a ingresar? ");
            tamaño = int.Parse(Console.ReadLine());
            numeros = new int[tamaño];
            mayor = 0;

        }
        
        public int ObtenerMayor( ) {
            
            for (int i = 0; i < tamaño; i++) {

                if (numeros[i] > mayor)
                {
                    mayor = numeros[i];
                }
                 
            }
         return mayor;
        }
       

        public void Llenado() {
            for (int i = 0; i < tamaño; i++) {
                Console.WriteLine("ingrese número {0}: ", i+1);
                numeros[i] = int.Parse(Console.ReadLine());
            }
        }
        public void MostrarMayor() {
            Console.WriteLine("el número mayor es {0}", ObtenerMayor());
        }
        

       
    }
}
