﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicios
{
    //2.	Crear un programa que lea un número entero y a partir de él cree un cuadrado de asteriscos con ese tamaño.Los asteriscos sólo 
    //    se verán en el borde del cuadrado, no en el interior.
    class Marco
    {
        private int tamaño;

        public Marco()
        {
            Console.WriteLine("Ingrese el tamaño del cuadrado: ");
            tamaño = int.Parse(Console.ReadLine());

        }
        public void Cuadrado() {
            if ( tamaño <= 15)
            {
                //linea superior
                for (int i = 0; i < tamaño; i++)
                {
                    Console.Write("*");
                }
                Console.WriteLine(" ");
                //parte media
                for (int i = 0; i < tamaño - 2; i++)
                {
                    Console.Write("*");
                    for (int j = 0; j < tamaño - 2; j++)
                    {
                        Console.Write(" ");
                    }
                    Console.WriteLine("*");
                }
                //linea inferior
                for (int i = 0; i < tamaño; i++)
                {
                    Console.Write("*");
                }
            }
            else
            {
                Console.WriteLine(" por favor ingresa un numero entre 0 y 15");
            }
        }
    }
}
