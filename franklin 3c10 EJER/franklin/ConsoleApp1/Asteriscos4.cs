﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicios
{
    //5.	Crear el programa que ingrese un numero para deterinar el tamaño PIRAMIDE
    class Asteriscos4
    {
        private int numero;
        private String adicion;

        public Asteriscos4()
        {
            Console.WriteLine("ingrese numero para determinar el tamaño de la piramide ");
            numero = int.Parse(Console.ReadLine());
            adicion = "*";
        }
        public void Piramide()
        {
            
            
            for (int i = 0; i < numero; i++) {

                for (int j = 1; j <= (numero - i); j++)
                {
                    Console.Write(" ");
                }
                Console.WriteLine( adicion);
                adicion += "**";

            }
        }

       
    }
}
